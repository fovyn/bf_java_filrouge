import { Component, OnInit } from '@angular/core';
import {ProduitService} from "../../services/produit.service";
import {Types} from "../../models/produit.model";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  constructor(private $service: ProduitService) { }

  ngOnInit(): void {
  }

  handleSubmit(): void {
    this.$service.create({nom: "", type: Types.Vetement, img: ""}).subscribe()
  }
}

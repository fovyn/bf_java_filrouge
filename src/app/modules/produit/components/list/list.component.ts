import { Component, OnInit } from '@angular/core';
import {ProduitService} from "../../services/produit.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private $service: ProduitService) { }

  ngOnInit(): void {
    this.$service.getAll().subscribe(data => console.log(data));
  }

}

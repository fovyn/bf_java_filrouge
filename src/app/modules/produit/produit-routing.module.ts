import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListComponent} from "./components/list/list.component";
import {AddComponent} from "./components/add/add.component";
import {EditComponent} from "./components/edit/edit.component";
import {ProduitComponent} from "./produit.component";

const routes: Routes = [
  { path: '', component: ProduitComponent, children: [
      { path: 'list', component: ListComponent },
      { path: 'add', component: AddComponent },
      { path: 'edit/:id', component: EditComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProduitRoutingModule { }

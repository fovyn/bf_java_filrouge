import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Produit} from "../models/produit.model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor(private $http: HttpClient) { }

  getAll(): Observable<Produit[]> {
    return this.$http.get<Produit[]>("http://localhost:3000/produits");
  }
  getOne(id: number): Observable<Produit> {
    return this.$http.get<Produit>("http://localhost:3000/produits/"+ id);
  }

  create(produit: Produit): Observable<Produit> {
    return this.$http.post<Produit>("http://localhost:3000/produits", produit);
  }
}

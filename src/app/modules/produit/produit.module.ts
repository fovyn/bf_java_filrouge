import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProduitRoutingModule } from './produit-routing.module';
import { ListComponent } from './components/list/list.component';
import { AddComponent } from './components/add/add.component';
import { EditComponent } from './components/edit/edit.component';
import { ProduitComponent } from './produit.component';


@NgModule({
  declarations: [
    ListComponent,
    AddComponent,
    EditComponent,
    ProduitComponent
  ],
  imports: [
    CommonModule,
    ProduitRoutingModule
  ],
  providers:[]
})
export class ProduitModule { }

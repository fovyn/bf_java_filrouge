export enum Types {
  Vetement= 'Vetement',
  Bijoux= 'Bijoux'
}
export type Produit = {
  id?: number,
  nom: string,
  type: Types,
  img: string
}

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PRODUITCREATE_FORM } from '../forms/produit.form';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  createForm = new FormGroup(PRODUITCREATE_FORM, []);

  constructor(private $http: HttpClient) { }

  ngOnInit(): void {
  }


  handleSubmit() {
    if(this.createForm.valid) {
      console.log(this.createForm.value);
      this.$http.post("http://localhost:3000/produits", this.createForm.value).subscribe(res => console.log(res));
    }
  }

  displayForm() {
    console.log(this.createForm.value);
  }
}

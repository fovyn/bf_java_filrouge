import {FormControl, Validators} from "@angular/forms";

export const REGISTER_FRM = {
  "username": new FormControl(null, [Validators.required]),
  "password": new FormControl(null, [Validators.required]),
  "passwordVerify": new FormControl(null, [Validators.required])
}

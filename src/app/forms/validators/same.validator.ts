import {AbstractControl, FormControl, FormGroup, ValidatorFn} from "@angular/forms";

export function same(field1Name: string, field2Name: string): ValidatorFn {
  return (control: AbstractControl) => {
    const fg = control as FormGroup;

    const field1: FormControl | null = fg.get(field1Name) as FormControl;
    const field2: FormControl | null = fg.get(field2Name) as FormControl;

    if (!field1) {
      return {'same': `Le champs ${field1Name} n'existe pas`};
    }
    if (!field2) {
      return {'same': `Le champs ${field2Name} n'existe pas`}
    }

    const value1 = field1.value;
    const value2 = field2.value;

    if (value1 != value2) {
      return {'same': `Les valeurs des champs {${field1Name}, ${field2Name}} ne correspondent pas`}
    }
    return null;
  }
}

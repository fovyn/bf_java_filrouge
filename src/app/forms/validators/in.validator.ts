import {AbstractControl, FormControl, FormGroup, ValidatorFn} from "@angular/forms";

export function inValidator(...items: string[]): ValidatorFn {
  return (control: AbstractControl) => {
    const value = control.value;

    const item = items.find(it => it == value);

    if (item != null) {
      return null;
    }
    return {'in': "La valeur n'est pas dans le range accepté"};
  }
}

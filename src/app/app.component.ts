import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SIGNIN_FORM } from './forms/sign-in.form';
import {REGISTER_FRM} from "./forms/register.form";
import {same} from "./forms/validators/same.validator";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loginForm: FormGroup = new FormGroup(SIGNIN_FORM);

  handleSignIn() {
    console.log(this.loginForm);
    console.log(this.loginForm.value);
    console.log(this.loginForm.valid);

    if (this.loginForm.valid) {
      console.log("FAIRE QQCH");
    } else {
      console.log("RIEN FAIRE");
      console.log(this.loginForm.errors, this.loginForm.get("username")?.errors)
    }
  }
}
